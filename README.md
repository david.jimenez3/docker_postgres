# Docker Postgres
Proyecto de docker que despliega una base de datos postgrtesSQL con datos iniciales, para esta implementación en la primera ejecución del contenedor se agrega un usuario adicional al root con los suficientes permisos para manipular la base de datos 'testdb' la cula tiene una tabla usuarios con 3 registros


# Prerequisitos
GIT: Software de versionamiento necesario para actualizar el proyecto. [Link de descarga.](https://github.com/git-for-windows/git/releases/download/v2.45.0.windows.1/Git-2.45.0-64-bit.exe)

Docker Desktop (windows) software para desplegar el contenedro del proyecto. [Link de descarga.](https://www.docker.com/products/docker-desktop/)

DBeaver. [Link Descarga](https://dbeaver.io/files/dbeaver-ce-latest-x86_64-setup.exe) DataBase Management System, permite conexiones a diferentes motores de bases de datos, entre ellos postgreSQL .

# Configuración y puesta en funcionamiento

## GIT

### Se recomienda configurar el [acceso ssh al repositorio GIT](https://docs.gitlab.com/ee/user/ssh.html), a fin de no solicitar contraseñas en cada peticion push / pull / fecth

Una vez instalado GIT:

Realizar clone al repositorio (configurado ssh):

```
git clone git@gitlab.com:david.jimenez3/docker_postgres.git
```
O si no tiene configurado ssh:
```
git clone https://gitlab.com/david.jimenez3/docker_postgres.git
```

## DOCKER
Instalar [Docker](https://docs.docker.com/engine/install/ubuntu/) en ubuntu.

Instalar [Docker](https://docs.docker.com/desktop/install/windows-install/) en windows.

Abrir la carpeta del proyecto mongo_docker que se generó al clonarlo y ejecutar el comando docker-compose up:
```
cd docker_postgres
docker-compose up
```

### Adminer 
Una ves terminada la generación del contenedor, en su entorno local, puede acceder al sitio web adminer [http://localhost:8080](http://localhost:8080) para administrar la base de datos.

Ingresar el usuario y contraseña web definidos en el archivo postgresql/docker-entrypoint-initdb.d/init-user-db.sh .env del proyecto.

![acceso_adminer](img_readme/adminer.png)


### DBeaver (windows)
Al ejecutar DBeaver Compass ingresar los datos de conexion:

1. Crear la Conexión y postgreSQL

![conexion_dbeaver1](img_readme/dbeaver1.png)

2. Ingresar los datos de conexión: Los datos de  usuario y contraseña estan definidos en las variables de entorno, estas se encuentran en el archivo postgresql/docker-entrypoint-initdb.d/init-user-db.sh del proyecto.
![conexion_dbeaver2](img_readme/dbeaver2.png)

3. COn erl boton Probar conexión debe obtener el siguiente mensaje:
![conexion_dbeaver3](img_readme/dbeaver3.png)

4. Luego de aceptar para cerrar el test de conexión, hacer click en el boton finalizar.

5. Se visualiza la base de datos:
![conexion_dbeaver4](img_readme/dbeaver4.png)
