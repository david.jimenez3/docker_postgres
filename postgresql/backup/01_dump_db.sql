--
-- PostgreSQL database dump
--

-- Dumped from database version 16.2 (Debian 16.2-1.pgdg120+2)
-- Dumped by pg_dump version 16.2 (Debian 16.2-1.pgdg120+2)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: usuarios; Type: TABLE; Schema: public; Owner: usuario_docker
--

CREATE TABLE public.usuarios (
    id integer NOT NULL,
    username character varying NOT NULL,
    email character varying NOT NULL,
    password character varying NOT NULL
);


ALTER TABLE public.usuarios OWNER TO usuario_docker;

--
-- Name: usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: usuario_docker
--

CREATE SEQUENCE public.usuarios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.usuarios_id_seq OWNER TO usuario_docker;

--
-- Name: usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: usuario_docker
--

ALTER SEQUENCE public.usuarios_id_seq OWNED BY public.usuarios.id;


--
-- Name: usuarios id; Type: DEFAULT; Schema: public; Owner: usuario_docker
--

ALTER TABLE ONLY public.usuarios ALTER COLUMN id SET DEFAULT nextval('public.usuarios_id_seq'::regclass);


--
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: usuario_docker
--

INSERT INTO public.usuarios(username, email, password)
VALUES 
  ('Tales de Mileto'  ,   'talesdm@gmail.com' , 'password'),
  ('Papo de Alejandría' , 'papoda@gmail.com'  ,  'password'),
  ('Hipaso de Metapono' , 'hipasodm@gmail.com'	,'password')
;

--
-- Name: usuarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: usuario_docker
--

SELECT pg_catalog.setval('public.usuarios_id_seq', 39, true);


--
-- Name: usuarios usuarios_pk; Type: CONSTRAINT; Schema: public; Owner: usuario_docker
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pk PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

