#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
	CREATE USER usuario_docker;
    ALTER USER usuario_docker WITH PASSWORD 'usuario_docker_pass';
	CREATE DATABASE testdb;
    ALTER DATABASE testdb OWNER TO usuario_docker;
	GRANT ALL PRIVILEGES ON DATABASE testdb TO usuario_docker;    
EOSQL


psql -v ON_ERROR_STOP=1 --username="usuario_docker" testdb < /tmp/01_dump_db.sql